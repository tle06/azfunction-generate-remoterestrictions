﻿function set-stringToBool {
    param (
        [Parameter(Mandatory=$True)]
        [ValidateSet("true","false","1","0")]
        [String]$bool

    )
    
    
    if (($bool -eq "true") -or ($bool -eq "1")) {
        return $true
    } else {
        return $false
    }

}

function set-CommaStringToCollection {
    param (
        [Parameter(Mandatory=$True)]
        [String[]]$value,
        [Parameter(Mandatory=$True)]
        [ValidateSet(",","/")]
        [String]$splitChart
    )
    
    $collection = @()

    $value.split($splitChart) | ForEach-Object {
    
        $collection += $_.trim()
    }

    return $collection
}

function get-ipMaskfromCidrAddress {
    param (
        [Parameter(Mandatory=$True)]
        [String[]]$cidrAddress
    )
    
    $collection = @()
    foreach ($item in $cidrAddress) {
        $element=@{}
        $element= $item.split("/")
        $collection += [PSCustomObject]@{
            ip = $element[0].trim()
            mask = $element[1].trim()
        }
    }

    return $collection
}

function get-isIpAddress {
    param (
        [Parameter(Mandatory=$True)]
        [String]$ip
    )

    try {
        $check = $ip -as [ipaddress] -as [Bool]
        return $check
    }
    catch {
        return $false
    }
    
}

function get-cidrAddressValidation {
    param (
        [Parameter(Mandatory=$True)]
        [String[]]$cidrAddress
    )
    
    $collection = @()
    $validation = @()
    $collection = get-ipMaskfromCidrAddress -cidrAddress $cidrAddress

    foreach ($item in $collection) {
        if ((get-isIpAddress $item.ip) -and (get-isIpAddress $item.mask)) {
            $check = $true
        } else {
            $check = $false
        }
    
        $validation += [PSCustomObject]@{
            ip = $item.ip
            mask= $item.mask
            cidr= "$($item.ip)/$($item.mask)"
            validated = $check
        }
    }

    return $validation
}


function get-data {
    param (
        [Parameter(Mandatory=$True)]
        [String]$addresses,
        [Parameter(Mandatory=$True)]
        [Boolean]$deny
    )

    $addr= set-CommaStringToCollection -value $addresses -splitChart ","
    $ipValidation = get-cidrAddressValidation -cidrAddress $addr
    $result = @()
    $debug = @()
    $errorMessage = ""
    
    if (-not ($ipValidation | Where-Object -Property "validated" -in $false)) {

        try {
            $data = Generate-RemoteRestrictions -globalDeny $deny -addresses $addr
        }
        catch {
            $errorMessage = "[FTC1] $($_.Exception.Message)"
        }
    } else {
        $errorMessage = "[FM1] IP value is not in CIDR format, example: 192.168.1.1/255.255.255.255`n"
        
    }

    $debug = [PSCustomObject]@{
        ArgCidr = $addresses
        ArgBool = $deny
        ResultCommaSplitCollection=$addr
        ResultIpValidation = $ipValidation
        Resultdata = $data
    }

    $result = [PSCustomObject]@{
        result = $data
        debug = $debug
        errorMessage = $errorMessage
    }

    return $result
}