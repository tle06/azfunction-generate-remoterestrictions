﻿
function get-testSuccess {
    param (
        [Parameter(Mandatory=$True)]
        [pscustomobject]$object
    )
    
    if (-not ($object | Where-Object -Property "success" -in $false)) {
        return $true
    } else {
        return $false
    }
}

function get-urlResult {
    param (
        [Parameter(Mandatory=$false)]
        [String]$ip,
        [Parameter(Mandatory=$false)]
        [String]$deny,
        [Parameter(Mandatory=$True)]
        [String]$url
    )
    
    $result=@()
    $d=""

    try {
        $d = Invoke-RestMethod -Method Get -Uri "$($url)?ip=$($ip)&deny=$($deny)"
        $check = $true
    }
    catch {
        $check = $false
        $d=$_.Exception
    }

    $result = [PSCustomObject]@{
        data = $d
        check = $check
    }

    return $result
}


function set-success {
    param (
        [Parameter(Mandatory=$True)]
        [bool]$origin,
        [Parameter(Mandatory=$True)]
        [bool]$checked
    )
    
    if ((($origin -eq $false) -and ($checked -eq $true)) -or (($origin -eq $true) -and ($checked -eq $false))) {
        return $true
    } else {
        return $false
    }
}

$url = "http://localhost:7071/api/HttpGenerateRemoteRestrictions"
$data = @()
$result = @()

#failure
$data += [PSCustomObject]@{ip = "";deny = "";failure = $true}
$data += [PSCustomObject]@{ip = "";deny = "0";failure = $true}
$data += [PSCustomObject]@{ip = "";deny = "1";failure = $true}
$data += [PSCustomObject]@{ip = "";deny = "true";failure = $true}
$data += [PSCustomObject]@{ip = "";deny = "false";failure = $true}
$data += [PSCustomObject]@{ip = "";deny = "test";failure = $true}

$data += [PSCustomObject]@{ip = "test";deny = "";failure = $true}
$data += [PSCustomObject]@{ip = "test";deny = "0";failure = $true}
$data += [PSCustomObject]@{ip = "test";deny = "1";failure = $true}
$data += [PSCustomObject]@{ip = "test";deny = "true";failure = $true}
$data += [PSCustomObject]@{ip = "test";deny = "false";failure = $true}
$data += [PSCustomObject]@{ip = "test";deny = "test";failure = $true}

$data += [PSCustomObject]@{ip = "192.168.1.1/255.255.255.256";deny = "";failure = $true}
$data += [PSCustomObject]@{ip = "192.168.1.1/255.255.255.256";deny = "0";failure = $true}
$data += [PSCustomObject]@{ip = "192.168.1.1/255.255.255.256";deny = "1";failure = $true}
$data += [PSCustomObject]@{ip = "192.168.1.1/255.255.255.256";deny = "true";failure = $true}
$data += [PSCustomObject]@{ip = "192.168.1.1/255.255.255.256";deny = "false";failure = $true}
$data += [PSCustomObject]@{ip = "192.168.1.1/255.255.255.256";deny = "test";failure = $true}

$data += [PSCustomObject]@{ip = "192.168.1.256/255.255.255.255";deny = "";failure = $true}
$data += [PSCustomObject]@{ip = "192.168.1.256/255.255.255.255";deny = "0";failure = $true}
$data += [PSCustomObject]@{ip = "192.168.1.256/255.255.255.255";deny = "1";failure = $true}
$data += [PSCustomObject]@{ip = "192.168.1.256/255.255.255.255";deny = "true";failure = $true}
$data += [PSCustomObject]@{ip = "192.168.1.256/255.255.255.255";deny = "false";failure = $true}
$data += [PSCustomObject]@{ip = "192.168.1.256/255.255.255.255";deny = "test";failure = $true}

$data += [PSCustomObject]@{ip = "192.168.1.1/255.255.255.a";deny = "";failure = $true}
$data += [PSCustomObject]@{ip = "192.168.1.1/255.255.255.a";deny = "0";failure = $true}
$data += [PSCustomObject]@{ip = "192.168.1.1/255.255.255.a";deny = "1";failure = $true}
$data += [PSCustomObject]@{ip = "192.168.1.1/255.255.255.a";deny = "true";failure = $true}
$data += [PSCustomObject]@{ip = "192.168.1.1/255.255.255.a";deny = "false";failure = $true}
$data += [PSCustomObject]@{ip = "192.168.1.1/255.255.255.a";deny = "test";failure = $true}

$data += [PSCustomObject]@{ip = "192.168.1.a/255.255.255.255";deny = "";failure = $true}
$data += [PSCustomObject]@{ip = "192.168.1.a/255.255.255.255";deny = "0";failure = $true}
$data += [PSCustomObject]@{ip = "192.168.1.a/255.255.255.255";deny = "1";failure = $true}
$data += [PSCustomObject]@{ip = "192.168.1.a/255.255.255.255";deny = "true";failure = $true}
$data += [PSCustomObject]@{ip = "192.168.1.a/255.255.255.255";deny = "false";failure = $true}
$data += [PSCustomObject]@{ip = "192.168.1.a/255.255.255.255";deny = "test";failure = $true}

$data += [PSCustomObject]@{ip = "192.168.1.a/255.255.255.255";deny = "";failure = $true}
$data += [PSCustomObject]@{ip = "192.168.1.a/255.255.255.255";deny = "0";failure = $true}
$data += [PSCustomObject]@{ip = "192.168.1.a/255.255.255.255";deny = "1";failure = $true}
$data += [PSCustomObject]@{ip = "192.168.1.a/255.255.255.255";deny = "true";failure = $true}
$data += [PSCustomObject]@{ip = "192.168.1.a/255.255.255.255";deny = "false";failure = $true}
$data += [PSCustomObject]@{ip = "192.168.1.a/255.255.255.255";deny = "test";failure = $true}

$data += [PSCustomObject]@{ip = "192.168.1.a/255.255.255.255,192.168.1.2/255.255.255.255";deny = "";failure = $true}
$data += [PSCustomObject]@{ip = "192.168.1.a/255.255.255.255,192.168.1.2/255.255.255.255";deny = "0";failure = $true}
$data += [PSCustomObject]@{ip = "192.168.1.a/255.255.255.255,192.168.1.2/255.255.255.255";deny = "1";failure = $true}
$data += [PSCustomObject]@{ip = "192.168.1.a/255.255.255.255,192.168.1.2/255.255.255.255";deny = "true";failure = $true}
$data += [PSCustomObject]@{ip = "192.168.1.a/255.255.255.255,192.168.1.2/255.255.255.255";deny = "false";failure = $true}
$data += [PSCustomObject]@{ip = "192.168.1.a/255.255.255.255,192.168.1.2/255.255.255.255";deny = "test";failure = $true}

$data += [PSCustomObject]@{ip = "192.168.1.a/255.255.255.255,192.168.1.2/255.255.255.255";deny = "";failure = $true}
$data += [PSCustomObject]@{ip = "192.168.1.a/255.255.255.255,192.168.1.2/255.255.255.255";deny = "test";failure = $true}
$data += [PSCustomObject]@{ip = "192.168.1.a/255.255.255.255,192.168.1.2/255.255.255.255";deny = "";failure = $true}
$data += [PSCustomObject]@{ip = "192.168.1.a/255.255.255.255,192.168.1.2/255.255.255.255";deny = "test";failure = $true}

$data += [PSCustomObject]@{ip = "192.168.1.1/255.255.255.255,192.168.1.2/255.255.255.256";deny = "";failure = $true}
$data += [PSCustomObject]@{ip = "192.168.1.1/255.255.255.255,192.168.1.2/255.255.255.256";deny = "0";failure = $true}
$data += [PSCustomObject]@{ip = "192.168.1.1/255.255.255.255,192.168.1.2/255.255.255.256";deny = "1";failure = $true}
$data += [PSCustomObject]@{ip = "192.168.1.1/255.255.255.255,192.168.1.2/255.255.255.256";deny = "true";failure = $true}
$data += [PSCustomObject]@{ip = "192.168.1.1/255.255.255.255,192.168.1.2/255.255.255.256";deny = "false";failure = $true}
$data += [PSCustomObject]@{ip = "192.168.1.1/255.255.255.255,192.168.1.2/255.255.255.256";deny = "test";failure = $true}

$data += [PSCustomObject]@{ip = "192.168.1.1/255.255.255.255,192.168.1.2/255.255.255.256";deny = "";failure = $true}
$data += [PSCustomObject]@{ip = "192.168.1.1/255.255.255.255,192.168.1.2/255.255.255.256";deny = "test";failure = $true}
$data += [PSCustomObject]@{ip = "192.168.1.1/255.255.255.255,192.168.1.2/255.255.255.256";deny = "";failure = $true}
$data += [PSCustomObject]@{ip = "192.168.1.1/255.255.255.255,192.168.1.2/255.255.255.256";deny = "test";failure = $true}
#Success

$data += [PSCustomObject]@{ip = "192.168.1.1/255.255.255.255";deny = "0";failure = $false}
$data += [PSCustomObject]@{ip = "192.168.1.1/255.255.255.255";deny = "1";failure = $false}
$data += [PSCustomObject]@{ip = "192.168.1.1/255.255.255.255";deny = "true";failure = $false}
$data += [PSCustomObject]@{ip = "192.168.1.1/255.255.255.255";deny = "false";failure = $false}

$data += [PSCustomObject]@{ip = "192.168.1.1/255.255.255.255,192.168.1.2/255.255.255.255";deny = "0";failure = $false}
$data += [PSCustomObject]@{ip = "192.168.1.1/255.255.255.255,192.168.1.2/255.255.255.255";deny = "1";failure = $false}
$data += [PSCustomObject]@{ip = "192.168.1.1/255.255.255.255,192.168.1.2/255.255.255.255";deny = "true";failure = $false}
$data += [PSCustomObject]@{ip = "192.168.1.1/255.255.255.255,192.168.1.2/255.255.255.255";deny = "false";failure = $false}



foreach ($item in $data) {

    $q = get-urlResult -url $url -ip $item.ip -deny $item.deny
    $r = set-success -origin $item.failure -checked $q.check

    $result += [PSCustomObject]@{
        ip = $item.ip
        deny = $item.deny
        failure=$item.failure
        check = $q.check
        result = $q.data
        success = $r
    }
}

$res = get-testSuccess -object $result
$result | export-csv "C:\Users\tletemplier\Downloads\result.csv"
$res