using namespace System.Net

# Input bindings are passed in via param block.
param($Request, $TriggerMetadata)

# ---------------------------------------------------------------------------- #
#          Interact with query parameters or the body of the request.          #
# ---------------------------------------------------------------------------- #

$deny = $request.query.deny
$ip = $request.query.ip
$body = ""
$data=""
$status = $false

# ---------------------------------------------------------------------------- #
#                                 Load function                                #
# ---------------------------------------------------------------------------- #

#Source: https://gist.github.com/guavadevelopment/7217d87943eb69ac60f0f96f3be00874

#generates the RemoteRestrictions registry value to for setting HKLM\SOFTWARE\Microsoft\WebManagement\Server\RemoteRestrictions
#to a set of allowed/dneied ip addresses. THe address specified with allow/deny based on the opposite of the globalDeny setting
#
#Examples:
#-Globally deny access and allow a specific ip through
#   Generate-RemoteRestrictions -globalDeny $True -addresses "1.1.1.1/255.255.255.255"
#
#-Globally deny access and allow 2 ip and subnets
#   Generate-RemoteRestrictions -globalDeny $True -addresses "1.1.1.0/255.255.255.0", "2.2.2.0/255.255.255.0"
#
#-Globally allow access and deny 2 ips and subnets
#   Generate-RemoteRestrictions -globalDeny $False -addresses "1.1.1.0/255.255.255.0", "2.2.2.0/255.255.255.0"

function Generate-IpOrSubnetString {
    Param ([string]$ip, [string]$ipTemplateString, [int] $ipOffset)
    $ipTemplate = [System.Convert]::FromBase64String($ipTemplateString);
    $ipParts = $ip.Split('.');
    $ipTemplate[$ipOffset] = [System.Convert]::ToByte($ipParts[0]);
    $ipTemplate[$ipOffset + 2] = [System.Convert]::ToByte($ipParts[1]);
    $ipTemplate[$ipOffset + 4] = [System.Convert]::ToByte($ipParts[2]);
    $ipTemplate[$ipOffset + 6] = [System.Convert]::ToByte($ipParts[3]);
    return [System.Convert]::ToBase64String($ipTemplate);
}

function Generate-AddressConfig {
    Param ([int]$index, [string]$ip, [string]$subnet, [bool]$allow)

    $ipTemplate = "AwEDAQMBAwEC";
    $ipConfig = Generate-IpOrSubnetString -ip $ip -ipTemplateString $ipTemplate -ipOffset 1;

    $subnetTemplate = "LgQD/wP/A/8D/wMA";
    $subnetConfig = Generate-IpOrSubnetString -ip $subnet -ipTemplateString $subnetTemplate -ipOffset 3;
    $addressConfig = [System.Convert]::FromBase64String("AQAAABkDAAAAAQAAAC4EAwEDAQMBAwECAAAALgQD/wP/A/8D/wMAAABn".Replace($ipTemplate, $ipConfig).Replace($subnetTemplate, $subnetConfig));
    [System.Buffer]::BlockCopy([System.BitConverter]::GetBytes($index), 0, $addressConfig, 0, 4);
    $addressConfig[$addressConfig.Count - 1] = If($allow) { 104 } else { 103 };
    return [System.Convert]::ToBase64String($addressConfig);
}

function Generate-RemoteRestrictions {
    Param ([bool]$globalDeny, [string[]] $addresses)

    $header = [System.Convert]::FromBase64String("/wEZAgAAAAEAAABnAgAAABkAAAAA");
    $addressCountBytes = [System.BitConverter]::GetBytes($addresses.Count);
    [System.Buffer]::BlockCopy($addressCountBytes, 0, $header, 17, 4);
    $header[11] = If($globalDeny) { 104 } else { 103 };
    $remoteRestrictions = [System.Convert]::ToBase64String($header);
    $index = 0;
    foreach ($address in $addresses) {
        $ip = $address.Split('/')[0];
        $subnet = $address.Split('/')[1];
        $remoteRestrictions += (Generate-AddressConfig -index $index -ip $ip -subnet $subnet -allow (-Not $globalDeny));
        $index++;
    }
    return $remoteRestrictions;
}
function set-stringToBool {
    param (
        [Parameter(Mandatory=$True)]
        [ValidateSet("true","false","1","0")]
        [String]$bool

    )
    
    
    if (($bool -eq "true") -or ($bool -eq "1")) {
        return $true
    } else {
        return $false
    }

}

function set-CommaStringToCollection {
    param (
        [Parameter(Mandatory=$True)]
        [String[]]$value,
        [Parameter(Mandatory=$True)]
        [ValidateSet(",","/")]
        [String]$splitChart
    )
    
    $collection = @()

    $value.split($splitChart) | ForEach-Object {
    
        $collection += $_.trim()
    }

    return $collection
}

function get-ipMaskfromCidrAddress {
    param (
        [Parameter(Mandatory=$True)]
        [String[]]$cidrAddress
    )
    
    $collection = @()
    foreach ($item in $cidrAddress) {
        $element=@{}
        $element= $item.split("/")
        $collection += [PSCustomObject]@{
            ip = $element[0].trim()
            mask = $element[1].trim()
        }
    }

    return $collection
}

function get-isIpAddress {
    param (
        [Parameter(Mandatory=$True)]
        [String]$ip
    )

    try {
        $check = $ip -as [ipaddress] -as [Bool]
        return $check
    }
    catch {
        return $false
    }
    
}

function get-cidrAddressValidation {
    param (
        [Parameter(Mandatory=$True)]
        [String[]]$cidrAddress
    )
    
    $collection = @()
    $validation = @()
    $collection = get-ipMaskfromCidrAddress -cidrAddress $cidrAddress

    foreach ($item in $collection) {
        if ((get-isIpAddress $item.ip) -and (get-isIpAddress $item.mask)) {
            $check = $true
        } else {
            $check = $false
        }
    
        $validation += [PSCustomObject]@{
            ip = $item.ip
            mask= $item.mask
            cidr= "$($item.ip)/$($item.mask)"
            validated = $check
        }
    }

    return $validation
}


function get-data {
    param (
        [Parameter(Mandatory=$True)]
        [String]$addresses,
        [Parameter(Mandatory=$True)]
        [Boolean]$deny
    )

    $addr= set-CommaStringToCollection -value $addresses -splitChart ","
    $ipValidation = get-cidrAddressValidation -cidrAddress $addr
    $result = @()
    $debug = @()
    $errorMessage = ""
    
    if (-not ($ipValidation | Where-Object -Property "validated" -in $false)) {

        try {
            $data = Generate-RemoteRestrictions -globalDeny $deny -addresses $addr
        }
        catch {
            $errorMessage = "[FTC1] $($_.Exception.Message)"
        }
    } else {
        $errorMessage = "[FM1] IP value is not in CIDR format, example: 192.168.1.1/255.255.255.255`n"
        
    }

    $debug = [PSCustomObject]@{
        ArgCidr = $addresses
        ArgBool = $deny
        ResultCommaSplitCollection=$addr
        ResultIpValidation = $ipValidation
        Resultdata = $data
    }

    $result = [PSCustomObject]@{
        result = $data
        debug = $debug
        errorMessage = $errorMessage
    }

    return $result
}

# ---------------------------------------------------------------------------- #
#                                     main                                     #
# ---------------------------------------------------------------------------- #

if (-not $ip) {
    $ip = $request.body.ip
}

if (-not $deny) {
    $deny = $request.body.deny
}

try {
    $deny = set-stringToBool $deny
    $errorTrigger = $false
}
catch {
    $body += "[TC0] DENY value is not in the following format: true, false, 1, 0`n"
    $errorTrigger = $true
}

if ((-not $errorTrigger) -and $ip) {
    try {
        $data = get-data -addresses $ip -deny $deny

        if($data.result) {
            $body = $data.result
            $status = $true
        } else {
            $body += "[M0] $($data.errorMessage)"
            $status = $false
        }
    }
    catch {
        $body += "[TC1] IP value is not in CIDR format, example: 192.168.1.1/255.255.255.255"
        $status = $false
    }



} else {
    if (-not $ip) {
        $body += "[M1] IP value is not in CIDR format, example: 192.168.1.1/255.255.255.255"
    }

    $status = $false
}

# ---------------------------------------------------------------------------- #
#                                  return data                                 #
# ---------------------------------------------------------------------------- #

if ($status) {
    $status = [HttpStatusCode]::OK
} else {
    $status = [HttpStatusCode]::BadRequest
}

# Associate values to output bindings by calling 'Push-OutputBinding'.
# $status = [HttpStatusCode]::BadRequest
# $status = [HttpStatusCode]::OK
Push-OutputBinding -Name Response -Value ([HttpResponseContext]@{
    StatusCode = $status
    Body = $body
})
